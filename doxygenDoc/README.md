# Doxygen documentation

## Folders and files
- `DoxyfileLinux` is the doxygen configuration file. It can be used for both linux and windows. 
- `docSources` the folders of the source files.
- `doc` the folder where the output (html or latex) files are when installed in a personnal computer.

## Adding multiple files to the same doxygen manual
To do so, list the files you want to add in the INPUT setting in the doxygen configuration file (in our case, DoxyfileLinux).

The order of the files specified in INPUT counts : the link at the root of the tree on the left will be in the same order as their respective files.

Each documentation file must start with `/*!` and end with `*/`.

## Integration in Gitlab
The procedure of integration and update of the documentation is in the file `.gitlab_ci.yml` of the main folder.
