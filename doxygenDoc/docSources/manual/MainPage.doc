
/*! 

\mainpage WIMS une plateforme en ligne d’exercices autocorrigés à données aléatoires

WIMS comprend de très nombreux exercices de différentes disciplines (principalement mathématiques mais pas seulement) et de différents niveaux. WIMS est un logiciel libre. Il permet l'écriture de ses propres exercices.

Le présent tutoriel a été rédigé à l'occasion de la crise du coronavirus 2020 pour permettre aux enseignants de démarrer rapidement avec l'utilisation de WIMS par duplication de classes existantes. Il n'aborde pas du tout la question de la conception de nouveaux exercices.

Il est écrit pour les utilisateurs du serveur de l'Université de Bourgogne Franche-Comté https://wims.univ-bfc.fr qui est ouvert à tous.

Il a été conçu à partir de données (documentation et bases d'exercices) fournies par la communauté WIMS qui se mobilise. 

Il a été construit à partir d'échanges et de contributions de Fabien Durand, Bruno Mifsud, David Rousseau, Cyrille Douriez, Youssef Boulhamane, l'IREM de Picardie et bien entendu Bernadette Perrin-Riou.

Il sera enrichi au cours des jours à venir. 

Des informations supplémentaires sont disponibles 
- sur la page de l'inspection académique d'Amiens http://maths.ac-amiens.fr/Assurer-la-continuite-pedagogique-en-mathematiques.html ,
- sur le site WIMSEDU https://wimsedu.info/ ,
- en particulier sur le site de MutuWIMS https://wimsedu.info/?page_id=5089 qui dispense de la formation à l'usage des nouveaux utilisateurs,
- et sur le site de l'inspection académique d'Aix-Marseille http://www.pedagogie.ac-aix-marseille.fr/jcms/c_10765359/fr/utiliser-wims-pour-la-continuite-pedagogique .

Les enseignants intéressés par l'utilisation de WIMS peuvent contacter l'auteur de ce tutoriel qui anime le groupe de travail WIMS BFC et agit pour la promotion de WIMS en Bourgogne Franche-Comté. 

\author Michel Lenczner michel.lenczner@univ-fcomte.fr

- \subpage Warning
- \subpage AvailableClasses 
- \subpage ClassCreationAndUsage 

\page Warning Avertissement

L'utilisation des boutons de navigation des navigateurs est à proscrire, il conduit à des comportements instables du logiciel. N'utilisez que les liens internes à la page.

\image html duplication15.png " " width=800cm

\page AvailableClasses Classes disponibles

l’IREM Picardie a créé des classes de mathématiques et récupéré des classes notamment de l'IREM de Marseille et de l'association WimsEdu permettant un accès autonomes aux élèves.

- mathématiques collège : 6ième, 5ième, 4ième et 3ième
- mathématiques Seconde (classe complète créée par l’IREM Picardie en 2018-2019)
- mathématiques Première technologique (projet 2019-2020 par l’IREM Picardie)
- mathématiques spécialité mathématiques de Première
- mathématiques TS

Elles ont été installées sur le site WIMS de l'UBFC https://wims.univ-bfc.fr sous l'intitulé "A DUPLIQUER".

Attention ! Notez bien que certaines de ces classes sont en phase de test et n’ont pas toujours été vérifiées pédagogiquement. Si vous décelez une anomalie, n’hésitez pas à nous contacter.

En suivant la procédure de duplication vous y trouverez d'autres classes déjà constituées, en particulier pour l'enseignent supérieur. 

\page ClassCreationAndUsage Construire une classe par duplication et l'utiliser 

- \subpage duplicationClass
- \subpage ConfigurationClass
- \subpage StudentRegistration
- \subpage UsingSheets
- \subpage StudentSide
- \subpage advancedLevel

\page duplicationClass Duplication d'une classe

Pour ouvrir une classe WIMS en utilisant une classe existante sur le serveur WIMS BFC https://wims.univ-bfc.fr/ , aller sur le serveur et cliquez sur Zone enseignants

\image html duplication1.png " " width=800cm

Pour créer une classe virtuelle, cliquer sur « en créer une »

\image html duplication2.png " " width=800cm

puis sur « Dupliquer une classe existante ».

\image html duplication3.png " " width=800cm

Choisissez la classe de votre choix parmi celles qui sont nommées « A DUPLIQUER »,

\image html duplication4.png " " width=800cm

le mot de passe est le même pour toutes ces classes : pythagore .

\image html duplication5.png " " width=800cm

Pour installer l'intégralité du contenu, cliquer sur « Intégrale »

\image html duplication6.png " " width=800cm

\page ConfigurationClass Configuration de votre classe

Commencez par lire les CGU (conditions générales d'utilisation) puis les accepter (indispensable).

\image html duplication7.png " " width=800cm

Remplissez les champs. 

\b Attention : pour cette classe vous ne pourrez plus modifier le nom de l'établissement et vos nom et prénom.

\image html duplication8.png " " width=500cm

\b Attention: **notez bien votre mot de passe**, celui-ci vous permettra d’administrer votre classe. Notez également celui de la classe que vous devrez fournir aux élèves pour qu’ils s’inscrivent dans votre classe.

Cliquez sur continuer, puis ré-entrez vos mots de passe.

\image html duplication9.png " " width=800cm

Consultez la boîte mail renseignée plus haut pour obtenir le code permettant la finalisation de la procédure, et saisissez le dans la fenêtre qui apparaît.

\image html duplication10.png " " width=650cm

Lisez bien les informations de la page qui suit, puis cliquez sur le premier lien.

\image html duplication11.png " " width=800cm

Votre classe est prête à être utilisée.

\page StudentRegistration Inscription des élèves à votre classe 

Il y a deux façons de faire. 

- Soit vous déclarez vous-même les noms des élèves, leurs identifiants et leurs mots de passe.
- Soit vous leur communiquez le mot de passe de la classe et ils s’y inscriront eux même. 

La première solution n’est adaptée que pour des élèves assez matures.

\section StudentRegistration1 Inscription des élèves par l'enseignant 

Cliquer sur « Gestion »

\image html duplication13.png " " width=800cm

puis « Gestion des participants », puis « Ajouter un participant ». 

Renseigner les champs pour chacun de vos élèves : nom, prénom, un identifiant (par exemple construit à partir de son nom et prénom), son mot de passe.

L’adresse électronique n’est pas nécessaire. 

Vous communiquerez ces informations aux élèves. Ils pourront changer leur mot de passe lors de leur connexion.

\section StudentRegistration2 Les élèves s'inscrivent eux même

L’enseignant communique le nom de la classe, le nom de l'enseignant et le mot de passe de la classe. L’élève peut se connecter et remplir lui-même le formulaire. Il devra conserver précieusement son mot de passe.

\page UsingSheets Utilisation élémentaire des feuilles d'exercices dupliquées

Il y a de nombreuses options de configurations possibles d'une feuille d'exercice ainsi que des exercices : système de notation, nombre de répétitions autorisées, temps maximum autorisé etc... 

Les feuilles fournies dans les classes "A DUPLIQUER" sont déjà paramétrées. Elles peuvent être utilisées telles quelles. 

\b Attention **toutefois** : les feuilles ont une date d’expiration, pour modifier cette date, cliquez sur une feuille, et dans l’onglet « informations générales, changez la date et enregistrez.

\image html duplication12.png " " width=800cm

Pour voir la classe telle que les élèves la voient cliquer sur "Vue des participants"

\image html duplication16.png " " width=800cm

Pour revenir à ce que les enseignants voient cliquer sur "Retour à la page pour enseignants".

Les résultats du travail des élèves (temps passé, nombre de répétitions, notes etc...) se trouvent en cliquant sur "Bilan des évaluations".

\page StudentSide Du côté de l'élève

\section StudentSide1 Comment entrer une expression mathématique dans les exercices 

Quand vous travaillez avec WIMS, vous aurez souvent besoin d'entrer des expressions mathématiques.

Ces expressions mathématiques peuvent être tapées de façon habituelle :

- 3*x+5 pour 3x+5
- sin(pi*x) pour \f$ \sin(\pi x)\f$ 
- y^3+1 pour \f$ y^3 + 1\f$ 
- (x+1)/(y-1) pour \f$ \frac{x+1}{y-1} \f$ 
- etc...

De plus, WIMS contient un analyseur intelligent capable de corriger des erreurs communes dans les expressions mathématiques. Par exemple, 3x+5 sera corrigé en 3*x+5, sin x corrigé en sin(x), etc. Mais nous ne vous conseillons pas de vous appuyer trop sur ce correcteur, car des ambiguïtés dans les expressions peuvent conduire parfois à des interprétations erronées. Il sera toujours mieux de taper les expressions de façon correcte, même si cela est parfois un peu contraignant.

Une liste de fonctions mathématiques et la façon (correcte) de les entrer se trouve dans l'onglet à gauche "Documentation" -> "Guide" -> section I.6 *Comment entrer une expression mathématique*. 

\section Calculator Usage de la calculatrice

WIMS est un outil puissant pour le renforcement d'automatismes de calcul. Utiliser une calculatrice serait absolument contre-productif. Leur usage est à proscrire.

\section WorkingPhilosophy Façon de travailler

\b Attention, certains exercices sont courts, mais la plupart nécessite de faire les calculs sur feuille.

Si vous ne comprenez pas et ne trouvez pas la solution tout seul, vous pouvez poser la question à d'autres.

L'expérience montre que les élèves / étudiants qui réussissent leur année sont ceux qui s'engagent dans le processus d'apprentissage. Un outil comme WIMS vous permet d'avoir un retour sur cet engagement. Vous pouvez vous tester vous même, vous rendre compte des points sur lesquels vous êtes incollable, mais aussi ceux qui vous posent problème.

Vos erreurs vous frustrent ? Vous en avez honte ? Elles vous découragent ? Ne vous laissez pas faire ! Se rendre compte d'une erreur est une très bonne chose ! C'est l'occasion d'apprendre du nouveau ! Quelle chance !

N'hésitez pas à poser des questions aux autres, à vous-même. Ne laissez pas tomber tant que vous n'avez pas compris.

Ensuite, pour que cette compréhension ne soit pas fugitive, vous devez vous entraîner jusqu'à ce que le point tout à l'heure difficile devienne connaissance acquise, maitrisée. 

\page advancedLevel Pour aller plus loin

WIMS offre de très grandes possibilités

- de programmation d'exercices, 
- de paramétrage de leur utilisation 
- de paramétrage de la notation
- et d'analyse des résultats.

L'enseignant intéressé pourra consulter la documentation de WIMS

- dans l'onglet "Documentation"
- mais aussi le wiki de WIMS EDU https://wiki.wimsedu.info/
- sur le site MutuWIMS https://wimsedu.info/?page_id=5089

*/


