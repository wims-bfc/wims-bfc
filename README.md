# Ce projet est archivé, il n'est plus actif.
[Accès au nouveau dépôt Gitlab WIMS BFC](https://gitlab.com/michelLenczner/wims-bfc)

# wims-bfc

Le [nouveau serveur WIMS BFC](https://wims.utbm.fr/wims/) installé à l'Université de Technologie de Belfort Montbéliard (UTBM). Sa liaison avec les Moodle d'établissement est possible, pour cela contacter Hélène Behague helene.behague@utbm.fr. Pour toute autre demande, contacter michel.lenczner@utbm.fr.

Data and tools produced by the WIMS BFC group and made available to everyone.

[Documentation WIMS BFC](https://wims-bfc.gitlab.io/wims-bfc).

[Répertoire du protocole de remédiation](https://gitlab.com/wims-bfc/wims-bfc/-/tree/master/documents). Il peut être chargé comme une archive avec le bouton de téléchargement en haut de la page, à gauche du bouton `clone`.

Le [serveur WIMS](https://wims.univ-bfc.fr/wims/) installé à l'Université de Bourgogne Franche-Comté ne sera plus maintenu. Vous êtes encouragé à migrer vos classes vers le nouveau serveur. 

## Contact

For further informations on WIMS BFC feel free to contact michel.lenczner@univ-fcomte.fr.

## Liens en relation avec le projet WIMS BFC

Une [vidéo](http://podcast.u-pem.fr/videos/?video=MEDIA200221113318678) de présentation du projet WIMS BFC faite à l'occasion de la journée "WIMS - Evolution" du 6 février 2020 à l'université Gustave Eiffel.

Le projet [PLaTon](https://premierlangage.github.io/PLaTon-web/) de plateforme open source d'exercices en lignes.

Le site du projet [RITM BFC](https://www.ubfc.fr/excellence/ritm-bfc/) qui soutient financièrement le projet WIMS BFC.

## Quelques ressources WIMS

Le site [WIMS EDU](https://wimsedu.info/) de la communauté WIMS.

Le [Wiki WIMS](https://wiki.wimsedu.info/doku.php?id=a).

[MutuWIMS](https://wimsedu.info/?page_id=5089) plateforme de mutualisation de création d'exercices.

Le site [WIMS Euler](https://euler.ac-versailles.fr/rubrique194.html) de l'académie de Versailles avec ses tutoriels et ses ressources.

Le site [WIMS de l'Académie de Lyon](https://maths.enseigne.ac-lyon.fr/spip/spip.php?article711) avec ses tutoriels.



