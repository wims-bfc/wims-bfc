# Ressources MutuWIMS

Ce dépôt contient une sélection des ressources de [MutuWIMS](https://wimsedu.info/?page_id=5089). Pour d'avantage d'information et de ressources se rendre sur son site.

[MutuWIMS](https://wimsedu.info/?page_id=5089) est à la fois une autoformation à WIMS, un recueil de ressources et un espace de travail collaboratif pour produire des sauvegarde adaptées à nos besoins. Cet espace est ouvert à toutes les disciplines et à tous les niveaux.

Il contient deux grandes parties : Autoformation à WIMS et Projets de Mutualisation.

## Collège : Sauvegardes du collège

- Sixième : 
`6eme_mutuwims_200826.tgz` En ligne depuis le 26 août 2020 Élaborée par la communauté.

- Cinquième :
`5eme_mutuwims_200826.tgz` En ligne depuis le 26 août 2020 Élaborée par la communauté.

- Quatrième :
`4eme_mutuwims_200826.tgz` En ligne depuis le 26 août 2020 Élaborée par la communauté.

- Troisième :
`3eme_GS_200826.tgz` En ligne depuis le 6 février 2020, mis à disposition par Grégory SCHMITT, GAP (merci !). Obtenu après plusieurs années d'utilisation dans sa classe. Ne pas hésiter à nous faire remonter des modifications ou des améliorations via le Forum et/ou rejoindre le groupe qui travaille sur cette classe.

## Lycée Sauvegardes du Lycée

- Seconde
`2nde_irem_picardie_200901.tgz` Programme 2019. En ligne depuis le 1 septembre 2020, conçu chez MutuWIMS à partir de la classe de l'IREM de PICARDIE, responsables Cyrille DOURIEZ et David ROUSSEAU , Version à compléter mais qui peut-être un bon point de départ. Ne pas hésiter à nous faire remonter des modifications ou des améliorations via le Forum et/ou rejoindre le groupe qui travaille sur cette classe.

- Premières :
  - `1spemath_mutuwims_200826.tgz` Première Générale Spécialité Math. En ligne depuis le 26 août 2020, conçu par la communauté MutuWIMS, version à compléter mais qui peut-être un bon point de départ. Ne pas hésiter à nous faire remonter des modifications ou des améliorations via le Forum et/ou rejoindre le groupe qui travaille sur cette classe.
  - `1technomath_irempicardie_200826.tgz` Première technologiques. Mise en ligne le 26 août 2020. Provient du travail de Cyrille DOURIEZ, IREM de Picardie.
  - ` 1_techno_tronc_commun_200318.tgz` Première technologique.

- Terminales

  - `term_gene_spe_math_200830.tgz` Terminale Générale Spécialité Math. 
En ligne depuis le 30 août 2020, conçu par la communauté MutuWIMS, version à compléter mais qui peut-être un bon point de départ. Ne pas hésiter à nous faire remonter des modifications ou des améliorations via le Forum et/ou rejoindre le groupe qui travaille sur cette classe.

  - `term_gene_math_compl_mutuwims200905.tgz` Terminale Générale Mathématiques complémentaires. En ligne depuis le 5 septembre 2020, conçu par la communauté MutuWIMS, version à compléter mais qui peut-être un bon point de départ. Ne pas hésiter à nous faire remonter des modifications ou des améliorations via le Forum et/ou rejoindre le groupe qui travaille sur cette classe.

  - `term_gene_math_expertes_mutuwims200905.tgz` Terminale Générale Mathématiques Expertes. En ligne depuis le 5 septembre 2020. Conçu par la communauté MutuWIMS, version à compléter mais qui peut-être un bon point de départ. Ne pas hésiter à nous faire remonter des modifications ou des améliorations via le Forum et/ou rejoindre le groupe qui travaille sur cette classe.


## Post BAC Sauvegardes du Postbac 

- `L1math_classeouverte_MCDavid.tgz` L1 Mathématiques, sauvegarde de la classe ouverte de Marie Claude DAVID.
